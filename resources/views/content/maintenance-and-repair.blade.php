@extends('layouts/contentLayoutMaster')

@section('title', 'Bakım ve Onarım')
@section('content')
<!-- Kick start -->


@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>

    <script>
        /*
        chartsDynamic('#myChart','bar','Elektriksel Güç',['Net Güç','İç Tüketim'],[65, 59])
        function chartsDynamic(element,type,labels,categories,data)
        {
            var options = {
                series: [{
                    name: labels,
                    data: data
                    }],
                chart: {
                    type: type,
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {
                    categories: categories,
                },
                fill: {
                    opacity: 1
                },
            };

            var chart = new ApexCharts(document.querySelector(element), options);

            chart.render();

            var date      = new Date();
            var year      = date.getFullYear();
            var month     = date.getMonth()+1;
            var day       = date.getDate();
            var fullDate  = year+'-'+month+'-'+day;
            var flatPicker = $('.flat-picker');

            flatPicker.each(function () {
                $(this).flatpickr({
                    mode: 'range',
                    locale: "tr",
                    defaultDate: [fullDate, fullDate]
                });
            });
        }

         */

    </script>

    <!--<script src="{{asset('assets/js/speedometer.js')}}"></script>-->
@endsection
<!-- Page layout -->
@endsection
