@extends('layouts/contentLayoutMaster')

@section('title', 'Raporlama')
@section('content')
<!-- Kick start -->



@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>


    <script>
        /*
        chartsDynamic('#myChart','bar','Elektriksel Güç',['Net Güç','İç Tüketim'],[65, 59])
        function chartsDynamic(element,type,labels,categories,data)
        {
            var options = {
                series: [{
                    name: labels,
                    data: data
                    }],
                chart: {
                    type: type,
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {
                    categories: categories,
                },
                fill: {
                    opacity: 1
                },
            };

            var chart = new ApexCharts(document.querySelector(element), options);

            chart.render();

            var date      = new Date();
            var year      = date.getFullYear();
            var month     = date.getMonth()+1;
            var day       = date.getDate();
            var fullDate  = year+'-'+month+'-'+day;
            var flatPicker = $('.flat-picker');

            flatPicker.each(function () {
                $(this).flatpickr({
                    mode: 'range',
                    locale: "tr",
                    defaultDate: [fullDate, fullDate]
                });
            });
        }

         */

    </script>

    <!--<script src="{{asset('assets/js/speedometer.js')}}"></script>-->
@endsection

<div class="card d-none">
  <div class="card-header">
    <h4 class="card-title">Kick start your next project 🚀</h4>
  </div>
  <div class="card-body">
    <div class="card-text">
      <p>
        Getting start with your project custom requirements using a ready template which is quite difficult and time
        taking process, Vuexy Admin provides useful features to kick start your project development with no efforts !
      </p>
      <ul>
        <li>
          Vuexy Admin provides you getting start pages with different layouts, use the layout as per your custom
          requirements and just change the branding, menu &amp; content.
        </li>
        <li>
          Every components in Vuexy Admin are decoupled, it means use use only components you actually need! Remove
          unnecessary and extra code easily just by excluding the path to specific SCSS, JS file.
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Kick start -->

<!-- Page layout -->
<div class="card d-none">
  <div class="card-header">
    <h4 class="card-title">What is page layout?</h4>
  </div>
  <div class="card-body">
    <div class="card-text">
      <p>
        Starter kit includes pages with different layouts, useful for your next project to start development process
        from scratch with no time.
      </p>
      <ul>
        <li>Each layout includes required only assets only.</li>
        <li>
          Select your choice of layout from starter kit, customize it with optional changes like colors and branding,
          add required dependency only.
        </li>
      </ul>
      <div class="alert alert-primary" role="alert">
        <div class="alert-body">
          <strong>Info:</strong> Please check the &nbsp;<a
            class="text-primary"
            href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/documentation/documentation-layouts.html#layout-collapsed-menu"
            target="_blank"
            >Layout documentation</a
          >&nbsp; for more layout options i.e collapsed menu, without menu, empty & blank.
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page layout -->
@endsection
