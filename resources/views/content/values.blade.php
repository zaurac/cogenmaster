@extends('layouts/contentLayoutMaster')

@section('title', 'Değerler')
@section('content')
<!-- Kick start -->

<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="table-responsive">
                <table class="table values">
                    <thead>
                    <tr>
                        <th>Sıra</th>
                        <th>İsim</th>
                        <th>Değer</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($values as $key => $name)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$name[1]}}</td>
                                <td data-key="{{$name[0]}}">0</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
    <script>
        valuesRealtime();

        setInterval(function()
        {
            valuesRealtime();
        },180000);

        function valuesRealtime()
        {
            var table = $('table.values tbody tr');

            for(var i=0; i < table.length;i++)
            {
                var keySelector = $('table.values tbody tr:eq('+i+') td:last-child');
                var key         = keySelector.data('key');


                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: '/speedMeter',
                    data: {
                        _token: token,
                        key: key
                    },
                    success: function (response)
                    {
                        if (response.result == 1)
                        {
                            var responseJson  = JSON.parse(response.response);
                            var factory_value = responseJson.factory_value;
                            var factory_key   = responseJson.factory_key;

                            $('[data-key="'+factory_key+'"]').text(factory_value);
                        }
                    }
                });
            }
        }
    </script>
@endsection

<div class="card d-none">
  <div class="card-header">
    <h4 class="card-title">Kick start your next project 🚀</h4>
  </div>
  <div class="card-body">
    <div class="card-text">
      <p>
        Getting start with your project custom requirements using a ready template which is quite difficult and time
        taking process, Vuexy Admin provides useful features to kick start your project development with no efforts !
      </p>
      <ul>
        <li>
          Vuexy Admin provides you getting start pages with different layouts, use the layout as per your custom
          requirements and just change the branding, menu &amp; content.
        </li>
        <li>
          Every components in Vuexy Admin are decoupled, it means use use only components you actually need! Remove
          unnecessary and extra code easily just by excluding the path to specific SCSS, JS file.
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Kick start -->

<!-- Page layout -->
<div class="card d-none">
  <div class="card-header">
    <h4 class="card-title">What is page layout?</h4>
  </div>
  <div class="card-body">
    <div class="card-text">
      <p>
        Starter kit includes pages with different layouts, useful for your next project to start development process
        from scratch with no time.
      </p>
      <ul>
        <li>Each layout includes required only assets only.</li>
        <li>
          Select your choice of layout from starter kit, customize it with optional changes like colors and branding,
          add required dependency only.
        </li>
      </ul>
      <div class="alert alert-primary" role="alert">
        <div class="alert-body">
          <strong>Info:</strong> Please check the &nbsp;<a
            class="text-primary"
            href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/documentation/documentation-layouts.html#layout-collapsed-menu"
            target="_blank"
            >Layout documentation</a
          >&nbsp; for more layout options i.e collapsed menu, without menu, empty & blank.
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page layout -->
@endsection
