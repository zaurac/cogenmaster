@extends('layouts/contentLayoutMaster')

@section('title', 'Ana Sayfa')
@section('content')
<!-- Kick start -->
<div class="row ">
    <div class="col-12">
        <section class="modern-horizontal-wizard">
            <div class="bs-stepper wizard-modern modern-wizard-example">
                <div class="bs-stepper-header">
                    <div class="step active" data-target="#all-factory" role="tab" id="all-factory-trigger">
                        <button type="button" onclick="tab('{{route('page.html',['content.factories.all-factory'])}}','#all-factory');clearInterval(engineOne);" class="step-trigger">
                            <span class="bs-stepper-box">
                                <i data-feather='octagon' class="font-medium-3"></i>
                            </span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Tesis Özeti</span>
                                <span class="bs-stepper-subtitle">Tüm tesislere ait bilgiler</span>
                            </span>
                        </button>
                    </div>
                    <div class="line">
                        <i data-feather='more-vertical' class="font-medium-2"></i>
                    </div>
                    <div class="step" data-target="#engine-one" role="tab" id="engine-one-trigger">
                        <button type="button" onclick="tab('{{route('page.html',['content.factories.genset-one'])}}','#engine-one');clearInterval(allFactory);" class="step-trigger">
                            <span class="bs-stepper-box">
                                <i data-feather='octagon' class="font-medium-3"></i>
                            </span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Genset 1</span>
                                <span class="bs-stepper-subtitle">1. motora ait bilgiler</span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="bs-stepper-content" style="background-color: transparent;box-shadow: none;padding: 0">
                    <div id="all-factory" class="content" role="tabpanel" aria-labelledby="all-factory-trigger"></div>
                    <div id="engine-one" class="content" role="tabpanel" aria-labelledby="engine-one-trigger"></div>
                </div>
            </div>
        </section>
    </div>
</div>

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
@endsection
@endsection
