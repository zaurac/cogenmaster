<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tesis Künyesi</h4>
            </div>
            <div class="card-body">
                <p><b>Lokasyon :</b> Başakşehir/İstanbul</p>
                <p><b>Kurulu güç :</b> 1 x 200 KW</p>
            </div>
        </div>
    </div>
    <div class="col-md-12"></div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Elektriksel Güç (kWh)</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="barChart('electric_work_counter',this.value,'Elektriksel Güç (kWh)','#electric_work_counter_trend');" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="electric_work_counter_trend" ></div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Enerji Trendi (kW)</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="lineChart('gen_active_power',this.value,'Enerji Trendi (kW)','#gen_active_power_three')" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex" id="gen_active_power_three" ></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Elektrik Üretim Trendi</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="gen_active_power_two" ></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Çalışma Saati</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="electric_work" ></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Jeneratör Aktif Gücü</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="gen_active_power_realtime" ></div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Availibility (Emre Amadelik) = Çalışılan Saat / Takvim Saati</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div class="col-md-4 border-1">
                        <h5 class="card-title">Genset 1</h5>
                        <div id="deneme1"></div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="card-title">Genset 2</h5>
                        <div id="deneme2"></div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="card-title">Genset 3</h5>
                        <div id="deneme3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Reliability (Güvenirlilik) = Üretilen kWh / Max kWh</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div class="col-md-4 border-1">
                        <h5 class="card-title">Genset 1</h5>
                        <div id="deneme4"></div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="card-title">Genset 2</h5>
                        <div id="deneme5"></div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="card-title">Genset 3</h5>
                        <div id="deneme6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>

    $('select[name="hoursSelect"]').trigger('change');
    lineChart('gen_active_power',3,'Enerji Trendi (kW)','#gen_active_power_two');
    speedMeter('operation_hour','Motor Çalışma Süresi');
    speedMeter('operation_hour_counter','Üretilen kWh / Max kWh');
    speedMeter('electric_work_counter','Elektirik Üretim Sayacı','#electric_work_counter_two');
    speedMeter('generator_reactive_power','Jeneratör Reaktif Gücü','#generator_reactive_power_two');
    speedMeter('main_frequency','Şebeke Frekansı','#main_frequency_two');
    speedMeter('main_frequency','Genset 1','#deneme1');
    speedMeter('main_frequency','Genset 2','#deneme2');
    speedMeter('main_frequency','Genset 3','#deneme3');
    speedMeter('main_frequency','Genset 1','#deneme4');
    speedMeter('main_frequency','Genset 2','#deneme5');
    speedMeter('main_frequency','Genset 3','#deneme6');
    speedMeter('main_voltage','Şebeke Voltajı','#main_voltage_two');
    barChart('electric_work',3,'Çalışma Saati','#electric_work');
    barChart('gen_active_power',3,'Jeneratör Aktif Gücü','#gen_active_power_realtime');

    var allFactory = setInterval(function()
    {
        lineChart('gen_active_power',3,'Enerji Trendi (kW)','#gen_active_power_two');
        speedMeter('operation_hour','Motor Çalışma Süresi');
        speedMeter('operation_hour_counter','Üretilen kWh / Max kWh');
        speedMeter('electric_work_counter','Elektirik Üretim Sayacı','#electric_work_counter_two');
        speedMeter('generator_reactive_power','Jeneratör Reaktif Gücü','#generator_reactive_power_two');
        speedMeter('main_frequency','Şebeke Frekansı','#main_frequency_two');
        speedMeter('main_voltage','Şebeke Voltajı','#main_voltage_two');
        barChart('electric_work',3,'Çalışma Saati','#electric_work');
        barChart('gen_active_power',3,'Jeneratör Aktif Gücü','#gen_active_power_realtime');
    },180000);
</script>
