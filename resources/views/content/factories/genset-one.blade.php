<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Genset Künyesi</h4>
            </div>
            <div class="card-body">
                <p><b>Genset Modeli :</b> TCG2020 V16</p>
                <p><b>Seri Numarası :</b> 123456</p>
                <p><b>Çalışma Saati :</b> 34534</p>
            </div>
        </div>
    </div>
    <div class="col-md-12"></div>


    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Silindir Sıcaklıkları L</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="combustion_chamber_l" ></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Silindir Sıcaklıkları R</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="combustion_chamber_r" ></div>
            </div>
        </div>
    </div>



    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Silindir Sıcaklıkları L</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="barChart('combustion_chamber_a1,combustion_chamber_a2,combustion_chamber_a3,combustion_chamber_a4,combustion_chamber_a5,combustion_chamber_a6,combustion_chamber_a7,combustion_chamber_a8',this.value,'Silindir Sıcaklığı A1,Silindir Sıcaklığı A2,Silindir Sıcaklığı A3,Silindir Sıcaklığı A4,Silindir Sıcaklığı A5,Silindir Sıcaklığı A6,Silindir Sıcaklığı A7,Silindir Sıcaklığı A8','#cylinder_temperature_l1_bar',1);" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="cylinder_temperature_l1_bar" ></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Silindir Sıcaklıkları R</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="barChart('combustion_chamber_b1,combustion_chamber_b2,combustion_chamber_b3,combustion_chamber_b4,combustion_chamber_b5,combustion_chamber_b6,combustion_chamber_b7,combustion_chamber_b8',this.value,'Silindir Sıcaklığı B1,Silindir Sıcaklığı B2,Silindir Sıcaklığı B3,Silindir Sıcaklığı B4,Silindir Sıcaklığı B5,Silindir Sıcaklığı B6,Silindir Sıcaklığı B7,Silindir Sıcaklığı B8','#cylinder_temperature_r1_bar',1);" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="cylinder_temperature_r1_bar" ></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Egzoz Sıcaklığı 1 - Egzoz Sıcaklığı 2</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="exhaust_temperature_before_catalyst_1_2" ></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">Egzoz Sıcaklığı 1 - Egzoz Sıcaklığı 2</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="lineChart('exhaust_temperature_before_catalyst,exhaust_temperature_after_catalyst',this.value,'Egzoz Sıcaklığı 1,Egzoz Sıcaklığı 2','#exhaust_temperature_before_catalyst_1_2_two')" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex" id="exhaust_temperature_before_catalyst_1_2_two" ></div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">HT Giriş - HT Çıkış</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex " id="heating_water_inlet_temp_heating_water_outlet_temp" ></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                <div class="header-left">
                    <h4 class="card-title">HT Giriş - HT Çıkış</h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                    <i data-feather="calendar"></i>
                    <select onchange="lineChart('heating_water_inlet_temp,heating_water_outlet_temp',this.value,'HT Giriş , HT Çıkış','#heating_water_inlet_temp_heating_water_outlet_temp_1')" name="hoursSelect" class="form-control border-0 shadow-none bg-transparent">
                        <option value="1">Son 1 Saat Ort.</option>
                        <option value="2">Son 24 Saat Ort.</option>
                        <option value="4">Son 1 Hafta Ort.</option>
                        <option value="5">Son 1 Ay Ort.</option>
                        <option value="6">Son 1 Yıl Ort.</option>
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div class="bar-chart-ex" id="heating_water_inlet_temp_heating_water_outlet_temp_1" ></div>
            </div>
        </div>
    </div>


</div>
<script>

    $('select[name="hoursSelect"]').trigger('change');
    barChart('combustion_chamber_a1,combustion_chamber_a2,combustion_chamber_a3,combustion_chamber_a4,combustion_chamber_a5,combustion_chamber_a6,combustion_chamber_a7,combustion_chamber_a8',3,'Silindir Sıcaklığı A1,Silindir Sıcaklığı A2,Silindir Sıcaklığı A3,Silindir Sıcaklığı A4,Silindir Sıcaklığı A5,Silindir Sıcaklığı A6,Silindir Sıcaklığı A7,Silindir Sıcaklığı A8','#combustion_chamber_l');
    barChart('combustion_chamber_b1,combustion_chamber_b2,combustion_chamber_b3,combustion_chamber_b4,combustion_chamber_b5,combustion_chamber_b6,combustion_chamber_b7,combustion_chamber_b8',3,'Silindir Sıcaklığı B1,Silindir Sıcaklığı B2,Silindir Sıcaklığı B3,Silindir Sıcaklığı B4,Silindir Sıcaklığı B5,Silindir Sıcaklığı B6,Silindir Sıcaklığı B7,Silindir Sıcaklığı B8','#combustion_chamber_r');
    barChart('exhaust_temperature_before_catalyst,exhaust_temperature_after_catalyst',3,'Egzoz Sıcaklığı 1,Egzoz Sıcaklığı 2','#exhaust_temperature_before_catalyst_1_2');
    barChart('heating_water_inlet_temp,heating_water_outlet_temp',3,'HT Giriş , HT Çıkış','#heating_water_inlet_temp_heating_water_outlet_temp');

    var engineOne = setInterval(function()
    {
        barChart('combustion_chamber_a1,combustion_chamber_a2,combustion_chamber_a3,combustion_chamber_a4,combustion_chamber_a5,combustion_chamber_a6,combustion_chamber_a7,combustion_chamber_a8',3,'Silindir Sıcaklığı A1,Silindir Sıcaklığı A2,Silindir Sıcaklığı A3,Silindir Sıcaklığı A4,Silindir Sıcaklığı A5,Silindir Sıcaklığı A6,Silindir Sıcaklığı A7,Silindir Sıcaklığı A8','#combustion_chamber_l');
        barChart('combustion_chamber_b1,combustion_chamber_b2,combustion_chamber_b3,combustion_chamber_b4,combustion_chamber_b5,combustion_chamber_b6,combustion_chamber_b7,combustion_chamber_b8',3,'Silindir Sıcaklığı B1,Silindir Sıcaklığı B2,Silindir Sıcaklığı B3,Silindir Sıcaklığı B4,Silindir Sıcaklığı B5,Silindir Sıcaklığı B6,Silindir Sıcaklığı B7,Silindir Sıcaklığı B8','#combustion_chamber_r');
        barChart('exhaust_temperature_before_catalyst,exhaust_temperature_after_catalyst',3,'Egzoz Sıcaklığı 1,Egzoz Sıcaklığı 2','#exhaust_temperature_before_catalyst_1_2');
        barChart('heating_water_inlet_temp,heating_water_outlet_temp',3,'HT Giriş , HT Çıkış','#heating_water_inlet_temp_heating_water_outlet_temp');
    },180000);

</script>
