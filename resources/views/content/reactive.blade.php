@extends('layouts/contentLayoutMaster')

@section('title', 'Reaktif Güç Analizi')

@section('content')
<!-- Kick start -->
<div class="row ">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Motor Çalışma Süresi</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="operation_hour"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Üretilen kWh / Max kWh</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="operation_hour_counter"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Elektirik Üretim Sayacı</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="electric_work_counter_two"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Jeneratör Reaktif Gücü</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="generator_reactive_power_two"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Şebeke Frekansı</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="main_frequency_two"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Şebeke Voltajı</h4>
            </div>
            <div class="card-body">
                <div class="card-text justify-content-center d-flex">
                    <div id="main_voltage_two"></div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('another-js')
    <script>
        speedMeter('operation_hour','Motor Çalışma Süresi');
        speedMeter('operation_hour_counter','Üretilen kWh / Max kWh');
        speedMeter('electric_work_counter','Elektirik Üretim Sayacı','#electric_work_counter_two');
        speedMeter('generator_reactive_power','Jeneratör Reaktif Gücü','#generator_reactive_power_two');
        speedMeter('main_frequency','Şebeke Frekansı','#main_frequency_two');
        speedMeter('main_voltage','Şebeke Voltajı','#main_voltage_two');

        var allFactory = setInterval(function()
        {
            speedMeter('operation_hour','Motor Çalışma Süresi');
            speedMeter('operation_hour_counter','Üretilen kWh / Max kWh');
            speedMeter('electric_work_counter','Elektirik Üretim Sayacı','#electric_work_counter_two');
            speedMeter('generator_reactive_power','Jeneratör Reaktif Gücü','#generator_reactive_power_two');
            speedMeter('main_frequency','Şebeke Frekansı','#main_frequency_two');
            speedMeter('main_voltage','Şebeke Voltajı','#main_voltage_two');

        },180000);
    </script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
@endsection
@endsection
