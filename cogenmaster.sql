-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 09 Ağu 2022, 08:44:53
-- Sunucu sürümü: 8.0.29
-- PHP Sürümü: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `menuqr`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `categories`
--

CREATE TABLE `categories` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int NOT NULL,
  `order` int NOT NULL DEFAULT '99',
  `status` smallint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `categories`
--

INSERT INTO `categories` (`id`, `title`, `image`, `slug`, `customer_id`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sıcak İçecekler', 'assets/images/categories/sicak_icecekler-1655062620.jpg', 'sicak-icecekler', 1, 3, 1, '2022-06-12 16:37:00', '2022-06-18 13:39:27'),
(2, 'Soğuk İçecekler', 'assets/images/categories/soguk_icecekler-1655062715.jpg', 'soguk-icecekler', 1, 2, 1, '2022-06-12 16:38:35', '2022-06-18 13:39:27'),
(3, 'Tavuklar', 'assets/images/categories/tavuklar-1655560997.png', 'tavuklar', 1, 4, 1, '2022-06-18 11:03:17', '2022-06-18 13:39:27'),
(4, 'Ana yemekler', 'assets/images/categories/ana_yemekler-1655561025.jpg', 'ana-yemekler', 1, 1, 1, '2022-06-18 11:03:45', '2022-06-18 13:39:27');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customers`
--

CREATE TABLE `customers` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` json DEFAULT NULL,
  `theme` smallint DEFAULT '1',
  `packet` smallint DEFAULT '1',
  `site_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_unit` smallint DEFAULT '1',
  `logo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` smallint DEFAULT NULL,
  `status` smallint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `customers`
--

INSERT INTO `customers` (`id`, `title`, `email`, `phone`, `address`, `facebook`, `instagram`, `twitter`, `youtube`, `color`, `theme`, `packet`, `site_title`, `slug`, `currency_unit`, `logo`, `favicon`, `payment_status`, `status`, `created_at`, `updated_at`, `about`) VALUES
(1, 'Zaurac Teknoloji', 'merhaba@zaurac.io', '05342233232', 'Çamlık Mahallesi Emre Sokak no:24 kat :2 daire : 7', NULL, NULL, NULL, NULL, '[\"#ca243f\", \"#000000\", \"#ffffff\"]', 1, 1, 'Zaurac Teknoloji', 'zaurac-teknoloji', 1, 'assets/images/customer/zaurac_teknoloji-logo-1654467471.png', 'assets/images/customer/zaurac_teknoloji-favicon-1654721248.png', 1, 1, '2022-05-14 14:43:01', '2022-06-19 15:32:40', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><b>Lorem Ipsum</b>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int UNSIGNED NOT NULL,
  `data_type_id` int UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'title', 'text', 'Ünvan', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(24, 4, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(25, 4, 'phone', 'text', 'Telefon', 0, 1, 1, 1, 1, 1, '{}', 5),
(26, 4, 'address', 'text', 'Adres', 0, 0, 1, 1, 1, 0, '{}', 6),
(27, 4, 'facebook', 'text', 'Facebook Linki', 0, 0, 1, 0, 0, 0, '{}', 7),
(28, 4, 'instagram', 'text', 'Instagram Linki', 0, 0, 1, 0, 0, 0, '{}', 8),
(29, 4, 'twitter', 'text', 'Twitter Linki', 0, 0, 1, 0, 0, 0, '{}', 9),
(30, 4, 'youtube', 'text', 'Youtube Linki', 0, 0, 1, 0, 0, 0, '{}', 10),
(31, 4, 'color', 'text', 'Tema Rengi', 0, 0, 1, 0, 0, 0, '{}', 11),
(32, 4, 'theme', 'select_dropdown', 'Tema', 0, 1, 1, 0, 0, 0, '{\"default\":\"Tema\",\"options\":{\"1\":\"Standart\"}}', 12),
(33, 4, 'packet', 'select_dropdown', 'Paket', 0, 1, 1, 0, 0, 0, '{\"default\":\"Paket\",\"options\":{\"1\":\"Standart\"}}', 13),
(34, 4, 'site_title', 'text', 'Site Title', 0, 0, 1, 1, 1, 1, '{}', 14),
(35, 4, 'slug', 'text', 'Site Linki', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"required|unique:customers,slug\"}}', 3),
(36, 4, 'currency_unit', 'select_dropdown', 'Para Birimi', 0, 0, 1, 1, 1, 0, '{\"default\":\"Para Birimi\",\"options\":{\"1\":\"TL (\\u20ba)\",\"2\":\"Dolar ($)\",\"3\":\"Euro (\\u20ac)\"}}', 15),
(37, 4, 'logo', 'image', 'Logo', 0, 1, 1, 1, 1, 0, '{}', 17),
(38, 4, 'favicon', 'image', 'Favicon', 0, 0, 1, 1, 1, 1, '{}', 18),
(39, 4, 'payment_status', 'checkbox', 'Ödeme Durumu', 0, 1, 1, 1, 1, 1, '{\"on\":\"Yap\\u0131ld\\u0131\",\"off\":\"Yap\\u0131lmad\\u0131\",\"checked\":false}', 19),
(40, 4, 'status', 'checkbox', 'Yayın Durumu', 0, 1, 1, 1, 1, 1, '{\"on\":\"Aktif\",\"off\":\"Pasif\",\"checked\":false}', 20),
(41, 4, 'created_at', 'timestamp', 'Ekleme Zamanı', 0, 0, 1, 0, 0, 1, '{}', 21),
(42, 4, 'updated_at', 'timestamp', 'Düzenlenme Zamanı', 0, 0, 1, 0, 0, 0, '{}', 22),
(43, 1, 'user_belongsto_customer_relationship', 'relationship', 'Müşteri', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"customers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(44, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 6),
(45, 1, 'customer_id', 'text', 'Customer Id', 0, 1, 1, 1, 1, 1, '{}', 12),
(46, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(47, 5, 'name_surname', 'text', 'Ad & Soyad', 0, 1, 1, 0, 0, 0, '{}', 3),
(48, 5, 'email', 'text', 'Email', 0, 1, 1, 0, 0, 0, '{}', 4),
(49, 5, 'phone', 'text', 'Telefon', 0, 1, 1, 0, 0, 0, '{}', 5),
(50, 5, 'subject', 'text', 'Konu', 0, 1, 1, 0, 0, 0, '{}', 6),
(51, 5, 'message', 'text', 'Mesaj', 0, 0, 1, 0, 0, 0, '{}', 7),
(52, 5, 'customer_id', 'text', 'Customer Id', 0, 1, 1, 0, 0, 0, '{}', 8),
(53, 5, 'created_at', 'timestamp', 'Ekleme Zamanı', 0, 1, 1, 0, 0, 0, '{}', 9),
(54, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(55, 5, 'support_belongsto_customer_relationship', 'relationship', 'Müşteri', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"customers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(56, 5, 'status', 'checkbox', 'Durum', 0, 1, 1, 1, 0, 0, '{\"on\":\"Aktif\",\"off\":\"Pasif\",\"checked\":false}', 10),
(57, 5, 'status_content', 'rich_text_box', 'Durum Açıklama', 0, 0, 1, 1, 0, 0, '{\"validation\":{\"rule\":\"required\"}}', 11),
(58, 4, 'about', 'rich_text_box', 'Hakkımızda Yazısı', 0, 0, 1, 1, 1, 0, '{}', 16);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `data_types`
--

CREATE TABLE `data_types` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'Kullanıcı', 'Kullanıcılar', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2022-05-06 13:03:16', '2022-07-13 16:07:52'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(4, 'customers', 'customers', 'Müşteri', 'Müşteriler', NULL, 'App\\Customer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2022-05-06 17:00:05', '2022-06-05 09:07:25'),
(5, 'supports', 'supports', 'Destek Talebi', 'Destek Talepleri', NULL, 'App\\Support', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2022-05-14 14:36:33', '2022-05-14 14:44:57');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menus`
--

CREATE TABLE `menus` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2022-05-06 13:03:16', '2022-05-06 13:03:16');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int UNSIGNED NOT NULL,
  `menu_id` int UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2022-05-06 13:03:16', '2022-05-06 13:03:16', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 5, '2022-05-06 13:03:16', '2022-05-06 17:06:56', 'voyager.media.index', NULL),
(3, 1, 'Kullanıcılar', '', '_self', 'voyager-person', '#000000', 12, 1, '2022-05-06 13:03:16', '2022-05-06 17:05:43', 'voyager.users.index', 'null'),
(4, 1, 'Kullanıcı Rolleri', '', '_self', 'voyager-lock', '#000000', 12, 2, '2022-05-06 13:03:16', '2022-05-06 17:05:51', 'voyager.roles.index', 'null'),
(5, 1, 'Araçlar', '', '_self', 'voyager-tools', '#000000', NULL, 5, '2022-05-06 13:03:16', '2022-05-14 14:42:00', NULL, ''),
(6, 1, 'Menü Düzeni', '', '_self', 'voyager-list', '#000000', 5, 1, '2022-05-06 13:03:16', '2022-05-06 17:06:14', 'voyager.menus.index', 'null'),
(7, 1, 'Veritabanı', '', '_self', 'voyager-data', '#000000', 5, 2, '2022-05-06 13:03:16', '2022-05-06 17:06:21', 'voyager.database.index', 'null'),
(8, 1, 'İkonlar', '', '_self', 'voyager-compass', '#000000', 5, 3, '2022-05-06 13:03:16', '2022-05-06 17:06:35', 'voyager.compass.index', 'null'),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2022-05-06 13:03:16', '2022-05-06 17:01:27', 'voyager.bread.index', NULL),
(10, 1, 'Ayarlar', '', '_self', 'voyager-settings', '#000000', 5, 6, '2022-05-06 13:03:16', '2022-05-06 17:06:58', 'voyager.settings.index', 'null'),
(11, 1, 'Müşteriler', '', '_self', 'voyager-people', '#000000', NULL, 2, '2022-05-06 17:00:05', '2022-05-06 17:05:00', 'voyager.customers.index', 'null'),
(12, 1, 'Kullanıcılar', '', '_self', 'voyager-person', '#000000', NULL, 4, '2022-05-06 17:05:29', '2022-05-14 14:42:00', NULL, ''),
(13, 1, 'Destek Talepleri', '', '_self', 'voyager-mail', '#000000', NULL, 3, '2022-05-14 14:36:33', '2022-05-14 14:42:00', 'voyager.supports.index', 'null');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(32, '2022_05_29_110454_create_categories', 2),
(33, '2022_05_31_195342_create_products', 2),
(41, '2022_06_21_191237_create_notifications', 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint UNSIGNED NOT NULL,
  `customer_id` int NOT NULL,
  `table_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `notification` smallint NOT NULL DEFAULT '1',
  `read` smallint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `notifications`
--

INSERT INTO `notifications` (`id`, `customer_id`, `table_number`, `content`, `status`, `notification`, `read`, `created_at`, `updated_at`) VALUES
(1, 1, '13', '12312sadd', 0, 0, 1, '2022-06-21 16:28:36', '2022-06-25 18:03:53'),
(2, 1, '13', 'nnnnn', 0, 0, 1, '2022-06-25 17:41:42', '2022-06-25 18:03:53'),
(3, 1, '11', 'adasd', 0, 0, 1, '2022-06-25 18:45:06', '2022-06-25 18:57:29'),
(4, 1, '11', 'adasd', 0, 0, 1, '2022-06-25 18:45:25', '2022-06-25 18:57:29'),
(5, 1, '4343', NULL, 0, 0, 1, '2022-06-25 18:47:02', '2022-06-25 18:57:29'),
(6, 1, '43', NULL, 0, 0, 1, '2022-06-25 18:57:41', '2022-06-28 16:53:08'),
(7, 1, '43', NULL, 0, 0, 1, '2022-06-25 18:57:52', '2022-06-28 16:53:08'),
(8, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:01:13', '2022-06-28 16:53:08'),
(9, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:01:24', '2022-06-28 16:53:08'),
(10, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:02:31', '2022-06-28 16:53:08'),
(11, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:04:00', '2022-06-28 16:53:08'),
(12, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:05:02', '2022-06-28 16:53:08'),
(13, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:05:45', '2022-06-28 16:53:08'),
(14, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:06:15', '2022-06-28 16:53:08'),
(15, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:06:49', '2022-06-28 16:53:08'),
(16, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:07:05', '2022-06-28 16:53:08'),
(17, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:13:18', '2022-06-28 16:53:08'),
(18, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:13:32', '2022-06-28 16:53:08'),
(19, 1, '12', NULL, 0, 0, 1, '2022-06-25 19:13:45', '2022-06-28 16:53:08'),
(20, 1, '122', NULL, 0, 0, 1, '2022-06-25 19:14:42', '2022-06-28 16:53:08'),
(21, 1, '122', NULL, 0, 0, 1, '2022-06-25 19:14:51', '2022-06-28 16:53:08'),
(22, 1, '43', NULL, 0, 0, 1, '2022-06-25 19:23:30', '2022-06-28 16:53:08'),
(23, 1, '13', NULL, 0, 0, 1, '2022-06-25 19:24:59', '2022-06-28 16:53:08'),
(24, 1, '13', NULL, 0, 0, 1, '2022-06-25 19:25:25', '2022-06-28 16:53:08'),
(25, 1, '13', NULL, 0, 0, 1, '2022-06-25 19:25:53', '2022-06-28 16:53:08'),
(26, 1, '123', NULL, 0, 0, 1, '2022-06-26 10:59:50', '2022-06-28 16:53:08'),
(27, 1, '12', NULL, 0, 0, 1, '2022-06-26 11:00:03', '2022-06-28 16:53:08'),
(28, 1, '112', NULL, 0, 0, 1, '2022-06-26 11:00:14', '2022-06-28 16:53:08'),
(29, 1, '112', NULL, 0, 0, 1, '2022-06-26 11:00:19', '2022-06-28 16:53:08'),
(30, 1, '112', NULL, 0, 0, 1, '2022-06-26 11:00:23', '2022-06-28 16:53:08'),
(31, 1, '123', '123', 0, 0, 1, '2022-06-28 16:54:57', '2022-06-28 17:00:19'),
(32, 1, '123', '123', 0, 0, 1, '2022-06-28 16:55:14', '2022-06-28 17:00:19'),
(33, 1, '123', '123', 0, 0, 1, '2022-06-28 16:55:28', '2022-06-28 17:00:19'),
(34, 1, '123', '123', 0, 0, 1, '2022-06-28 16:59:07', '2022-06-28 17:00:19'),
(35, 1, '123', '123', 0, 0, 1, '2022-06-28 17:00:10', '2022-06-28 17:00:19'),
(36, 1, '123', '123', 1, 0, 0, '2022-06-28 17:00:47', '2022-06-28 17:00:53'),
(37, 1, '123', '123', 1, 0, 0, '2022-06-28 17:02:09', '2022-06-28 17:02:12'),
(38, 1, '123', '123', 1, 0, 0, '2022-06-28 17:04:58', '2022-06-28 17:04:58'),
(39, 1, '123', '123', 1, 0, 0, '2022-06-28 17:07:56', '2022-06-28 17:07:57');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ibrahim.belkir@zaurac.io', '$2y$10$rwBTIFXjbNfCXzgeVHGtKeZfgVhQ54vwRn.0d7XBTyFYV9WeJxjv6', '2022-05-24 16:59:44'),
('ibrahim.belkir@zaurac.io', 'RlssQxWioIu6YmsTdxDKJOboLkbUyugaO6iu2OwIZdZ38g5nkjCUwdFgcm3ufP5d', '2022-05-25 17:24:11'),
('ibrahim.belkir@zaurac.io', 'iaw8k7MpSDqm1vB11aeTENHYqA2OPOKu9O2N0MECTvSs7VOdw6BsfgwktpF6pvGu', '2022-05-25 17:24:32'),
('ibrahim.belkir@zaurac.io', 'lFL3m3E6Gm0TorVgUtYTGORK2l8mvFeJIjBeLCC3w8vJy6PnVEOm8fxKT7LrU4n4', '2022-05-25 17:25:00'),
('ibrahim.belkir@zaurac.io', '4mnvXOzcYcmTyPdC3OgeO3a1Qhw4dojvzH7ra5rwWm8PSe1xpen8AQY7fgoHFNa3', '2022-05-25 17:25:47'),
('ibrahim.belkir@zaurac.io', 'PBh5xOcZSoBXhlSfR9bSVbChWULqkxrrpvGQQa2RlfRI0eaMKZZWrdvepZ6rMemP', '2022-05-25 17:46:31'),
('ibrahim.belkir@zaurac.io', 'Yn0kwHfDKlrFE7NN2XFMcHJIUdzeujmXcFdRv9LE7vlrc5MSCmgQFq8MLfONWHDz', '2022-05-25 17:47:53'),
('ibrahim.belkir@zaurac.io', 'yWFWjdjVy5Yf7pT1RV8e5QsU9o2oxSlytvmZAPXzUY7FrGlwyjXOXaSaOFTtVnO1', '2022-05-25 17:51:03'),
('ibrahim.belkir@zaurac.io', 'PfxVMOI6j2aINrHQBqXmsrogZB5KFIo4EJSGLenMAU2s0Gy1ByIIshP2rXSQ5BHJ', '2022-05-25 17:51:25'),
('ibrahim.belkir@zaurac.io', 'dlqDtTlCOQS5nn9r66UufzP5s7JbMH5onhIgM3DsDw4UkqI5sWuEIeSV7et7Qhuh', '2022-05-25 17:51:29'),
('ibrahim.belkir@zaurac.io', '4ZETiObS8RXPxGXYjfbPAQFZf68Eg9aD1Fcu2Pve6AP4Sd3ngpR7QcLrHTkYwmgY', '2022-05-25 17:51:58'),
('ibrahim.belkir@zaurac.io', 'rTXZTDoRhiM5FsTmWJyP4NmiGbUVrSejNW6NARV3D5mrH0UKfTTeMHlsqvj4sJNd', '2022-05-25 18:05:58'),
('ibrahim.belkir@zaurac.io', 'Jc1m54Gkl1eZ3AaLFOASUm2Mmjz7b8vPHbpR2S5DkzJCmJFuGiBKAH4Iob8dFY78', '2022-05-25 18:10:59'),
('ibrahim.belkir@zaurac.io', 'SpEE8SLOejAENF0N3gNrWIULH1nmgVMvM7DYND6jt6rrZjuP9CHjxZMNbYvFm3h8', '2022-05-25 18:12:11');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(2, 'browse_bread', NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(3, 'browse_database', NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(4, 'browse_media', NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(5, 'browse_compass', NULL, '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(6, 'browse_menus', 'menus', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(7, 'read_menus', 'menus', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(8, 'edit_menus', 'menus', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(9, 'add_menus', 'menus', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(10, 'delete_menus', 'menus', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(11, 'browse_roles', 'roles', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(12, 'read_roles', 'roles', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(13, 'edit_roles', 'roles', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(14, 'add_roles', 'roles', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(15, 'delete_roles', 'roles', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(16, 'browse_users', 'users', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(17, 'read_users', 'users', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(18, 'edit_users', 'users', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(19, 'add_users', 'users', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(20, 'delete_users', 'users', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(21, 'browse_settings', 'settings', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(22, 'read_settings', 'settings', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(23, 'edit_settings', 'settings', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(24, 'add_settings', 'settings', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(25, 'delete_settings', 'settings', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(26, 'browse_customers', 'customers', '2022-05-06 17:00:05', '2022-05-06 17:00:05'),
(27, 'read_customers', 'customers', '2022-05-06 17:00:05', '2022-05-06 17:00:05'),
(28, 'edit_customers', 'customers', '2022-05-06 17:00:05', '2022-05-06 17:00:05'),
(29, 'add_customers', 'customers', '2022-05-06 17:00:05', '2022-05-06 17:00:05'),
(30, 'delete_customers', 'customers', '2022-05-06 17:00:05', '2022-05-06 17:00:05'),
(31, 'browse_supports', 'supports', '2022-05-14 14:36:33', '2022-05-14 14:36:33'),
(32, 'read_supports', 'supports', '2022-05-14 14:36:33', '2022-05-14 14:36:33'),
(33, 'edit_supports', 'supports', '2022-05-14 14:36:33', '2022-05-14 14:36:33'),
(34, 'add_supports', 'supports', '2022-05-14 14:36:33', '2022-05-14 14:36:33'),
(35, 'delete_supports', 'supports', '2022-05-14 14:36:33', '2022-05-14 14:36:33');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `products`
--

CREATE TABLE `products` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category_id` int NOT NULL,
  `customer_id` int NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int NOT NULL DEFAULT '99',
  `status` smallint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `summary`, `content`, `category_id`, `customer_id`, `slug`, `image`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Çay', '10', 'Çay', '<p>&Ccedil;ay a&ccedil;ıklama</p>', 1, 1, 'cay', 'assets/images/products/cay-1655062841.jpg', 2, 1, '2022-06-12 16:40:41', '2022-06-12 16:43:41'),
(2, 'Coca Kola', '20', 'Kola', '<p>Kola a&ccedil;ıklama</p>', 2, 1, 'coca-kola', 'assets/images/products/coca_kola-1655063016.png', 1, 1, '2022-06-12 16:43:36', '2022-06-12 16:43:41'),
(3, 'Fanta', '20', NULL, NULL, 2, 1, 'fanta', 'assets/images/products/fanta-1655229385.jpg', 3, 1, '2022-06-14 14:56:25', '2022-06-26 08:15:16'),
(4, 'aaaaaa', '123', 'asdasd', '<p>asdasda</p>', 4, 1, 'aaaaaa', 'assets/images/products/aaaaaa-1655573612.png', 4, 1, '2022-06-18 14:33:32', '2022-06-26 08:15:16');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2022-05-06 13:03:16', '2022-05-06 13:03:16'),
(2, 'user', 'Normal User', '2022-05-06 13:03:16', '2022-05-06 13:03:16');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `settings`
--

CREATE TABLE `settings` (
  `id` int UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/June2022/KqukAQeuMVJpRioS65qw.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Admin Paneli', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Admin paneline hoşgeldiniz....', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/June2022/LuSNKUeabghasMOHryjF.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `supports`
--

CREATE TABLE `supports` (
  `id` int UNSIGNED NOT NULL,
  `name_surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `customer_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` smallint DEFAULT '1',
  `status_content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `supports`
--

INSERT INTO `supports` (`id`, `name_surname`, `email`, `phone`, `subject`, `message`, `customer_id`, `created_at`, `updated_at`, `status`, `status_content`) VALUES
(1, 'Halil İbrahim BELKIR', 'merhaba@zaurac.io', '5342233232', 'Deneme Konusu', 'menülerim görünmüyor', 1, '2022-05-14 17:43:20', '2022-05-14 14:45:25', 0, '<p>İşlem başarılı oldu</p>');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `translations`
--

CREATE TABLE `translations` (
  `id` int UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `customer_id`) VALUES
(1, 1, 'Halil İbrahim BELKIR', 'ibrahim.belkir@zaurac.io', 'users/June2022/eHXTabclZweooXkU8zgw.png', NULL, '$2y$10$NNxcbDn4PKHGoYiDGDYoHetEcJb/Qs7zYxrSzUQiqLMfCbV3mx8I6', 'LnjV0fqoqfgplsa4wf7oOQUUXaYiSJ0VCIuR1i3ScoPIoyX3ygXX286nRBHt', '{\"locale\":\"tr\"}', '2022-05-06 13:16:41', '2022-06-28 17:54:41', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Tablo için indeksler `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Tablo için indeksler `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Tablo için indeksler `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Tablo için indeksler `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Tablo için indeksler `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Tablo için indeksler `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Tablo için indeksler `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Tablo için indeksler `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Tablo için indeksler `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supports_customer_id_index` (`customer_id`);

--
-- Tablo için indeksler `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Tablo için indeksler `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Tablo için AUTO_INCREMENT değeri `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Tablo için AUTO_INCREMENT değeri `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Tablo için AUTO_INCREMENT değeri `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Tablo için AUTO_INCREMENT değeri `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Tablo için AUTO_INCREMENT değeri `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Tablo için AUTO_INCREMENT değeri `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Tablo kısıtlamaları `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
