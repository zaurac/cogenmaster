<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaterkitController;
use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Auth::routes();


Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login.index');
Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('/password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('/password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('password.email');
Route::get('password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::get('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'showConfirmForm'])->name('password.confirm');
Route::post('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'confirm']);

Route::middleware(['auth'])->group(function ()
{
    Route::get('dashboard', [StaterkitController::class, 'home'])->name('home');
    Route::get('reactive', [StaterkitController::class, 'reactive'])->name('reactive');
    Route::post('speedMeter', [StaterkitController::class, 'speedMeter'])->name('speedMeter');
    Route::post('lineChart', [StaterkitController::class, 'lineChart'])->name('electric_work_counter');
    Route::get('canli-tesis-izleme', [StaterkitController::class, 'liveFactory'])->name('liveFactory');
    Route::get('bakim-ve-onarim', [StaterkitController::class, 'maintenanceAndRepair'])->name('maintenanceAndRepair');
    Route::get('raporlama', [StaterkitController::class, 'reporting'])->name('reporting');
    Route::get('tavsiyeler', [StaterkitController::class, 'advices'])->name('advices');
    Route::get('konfigurasyon', [StaterkitController::class, 'configuration'])->name('configuration');
    Route::get('degerler', [StaterkitController::class, 'values'])->name('values');
    Route::get('page-html/{view}', [StaterkitController::class, 'pageHtml'])->name('page.html');
});
