$(document).ready(function ()
{
    $('.bs-stepper-header .step.active button.step-trigger').trigger('click');
    setTimeout(function ()
    {
        location.reload();
    },300000)
});

var loadingHtml     = '<div class="spinner-grow text-primary" role="status"></div>';
var cardLoadingHtml = '<div class="col-md-12"> <div class="card"><div class="card-body text-center" style="display: flex; align-items: center; justify-content: center;">'+loadingHtml+'</div></div></div>'

function tab(url,selector,otherInterval)
{
    var token = $('meta[name="csrf-token"]').attr('content');
    $('.bs-stepper-content .content').removeClass('active');
    $('.bs-stepper-header .step').removeClass('active');
    $('.bs-stepper-content .content').html('');
    $('.bs-stepper-content '+selector).addClass('active');
    $('.bs-stepper-header [data-target="'+selector+'"]').addClass('active');
    $('.bs-stepper-content '+selector).html(cardLoadingHtml);

    $.ajax({
        type: "get",
        dataType: "json",
        url: url,
        data:
        {
            _token: token,
        },
        success: function (response)
        {
            //var responseJson  = JSON.parse(response.response);
            $(selector).html(response.response);
        }
    });
}

function speedMeter(key,label,selector = '')
{
    if (selector != '')
    {
        var newSelector = selector;
    }
    else
    {
        var newSelector = '#'+key;
    }

    $(newSelector).html(cardLoadingHtml);
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "post",
        dataType: "json",
        url: '/speedMeter',
        data: {
            _token: token,
            key: key
        },
        success: function(response)
        {
            if (response.result == 1)
            {
                var responseJson  = JSON.parse(response.response);
                var data          = responseJson.factory_value;
                data              = parseInt(data);

                console.log(data);

                $(newSelector).html('');

                var options = {
                    chart: {
                        height: 350,
                        type: "radialBar",
                    },
                    series: [data],
                    noData: {
                        text: 'Yükleniyor...'
                    },
                    labels: [label],
                    plotOptions: {
                        radialBar: {
                            startAngle: -90,
                            endAngle: 90,
                            track: {
                                startAngle: -90,
                                endAngle: 90,
                            },
                            dataLabels: {
                                name: {
                                    show: true,
                                    offsetY: 0,
                                    fontSize: '10px',
                                    position:'back',
                                    color : "#c7253d"
                                },
                                value: {
                                    formatter: function(val) {
                                        return parseInt(val);
                                    },
                                    fontSize: "20px",
                                    show: true,
                                    offsetY: 10,
                                },
                            }
                        }
                    },
                    fill: {
                        type: "gradient",
                        gradient: {
                            shade: "light",
                            shadeIntensity: 1,
                            type: "horizontal",
                            stops: [0, (Number(data)+100)],
                            colorStops: [
                                {
                                    offset: 0,
                                    color: "#20E647",
                                    opacity: 1
                                },
                                {
                                    offset: 30,
                                    color: "#FDD835",
                                    opacity: 1
                                },
                                {
                                    offset: 70,
                                    color: "#FDD835",
                                    opacity: 1
                                },
                                {
                                    offset: 100,
                                    color: "#c7253d",
                                    opacity: 1
                                }
                        ]
                        }
                    },
                    stroke: {
                        lineCap: "butt"
                    },
                };
                var chart = new ApexCharts(document.querySelector(newSelector), options);

                chart.render();
            }
        }
    });
}


function lineChart(key,value,name,element)
{
    var token = $('meta[name="csrf-token"]').attr('content');
    $(element).html(cardLoadingHtml);
    $.ajax({
        type: "post",
        dataType: "json",
        url: '/lineChart',
        data: {
            _token: token,
            value: value,
            key: key,
            name :name,
        },
        success: function(response)
        {
            var responseJson  = JSON.parse(response.response);
            $(element).html('');
            if (responseJson != null)
            {
                var categories = [];

                if (value == 3)
                {
                    var oldSeries = [];
                    var names     = name.split(',');

                    $.each(responseJson, function(i, item)
                    {
                        oldSeries[i] = item.factory_value;
                    });

                    var series = [{ data : oldSeries,name: 'Değer'}];

                    $.each(names, function(i, item)
                    {
                        categories[i] = item;
                    });
                }
                else
                {
                    $.each(responseJson, function(i, item)
                    {
                        categories[i] = item.time;
                    });
                    var series = JSON.parse(response.series);
                }

                var options = {
                    series: series,
                    chart: {
                        type: 'line',
                        height: 350,
                        zoom: {
                            enabled: false
                        }
                    },
                    colors:JSON.parse(response.colors),
                    plotOptions: {
                        bar: {
                            borderRadius: 4,
                            horizontal: true,
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    xaxis: {
                        categories: categories,
                    }
                };
                var chart = new ApexCharts(document.querySelector(element), options);
                chart.render();
            }
            else
            {
                $(element).html('<div class="alert alert-primary" role="alert"><div class="alert-body">Seçtiğiniz seçeneğe göre veri bulunamadı.</div></div>');
            }
        }
    });
/*
    var options = {
        series: [{
            data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                borderRadius: 4,
                horizontal: true,
            }
        },
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
                'United States', 'China', 'Germany'
            ],
        }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();

 */
}

function barChart(key,value,name,element,status = 0)
{
    var token = $('meta[name="csrf-token"]').attr('content');
    $(element).html(cardLoadingHtml);

    $.ajax({
        type: "post",
        dataType: "json",
        url: '/lineChart',
        data: {
            _token: token,
            value: value,
            key: key,
            name:name,
            status:status,
        },
        success: function(response)
        {
            var responseJson  = JSON.parse(response.response);
            $(element).html('');
            if (responseJson != null)
            {
                var categories = [];
                var names      = name.split(',');

                if (names.length == 1)
                {
                    var widthColumn = '7%';
                }
                else if (names.length == 2)
                {
                    var widthColumn = '12%';
                }
                else if (names.length == 3)
                {
                    var widthColumn = '17%';
                }
                else if (names.length == 4)
                {
                    var widthColumn = '17%';
                }
                else
                {
                    var widthColumn = '50%';
                }

                if (value == 3 || status == 1)
                {
                    var oldSeries = [];

                    $.each(responseJson, function (i, item) {
                        oldSeries[i] = item.factory_value;
                    });

                    var series = [{data: oldSeries, name: 'Değer'}];

                    $.each(names, function (i, item) {
                        categories[i] = item;
                    });
                }
                else
                {
                    $.each(responseJson, function(i, item)
                    {
                        categories[i] = item.time;
                    });

                    var series = JSON.parse(response.series);

                }



                var options = {
                    series: series ,
                    chart: {
                        type: 'bar',
                        height: 350,
                        zoom: {
                            enabled: false
                        }
                    },
                    colors: JSON.parse(response.colors),
                    plotOptions: {
                        bar: {
                            borderRadius: 5,
                            horizontal: false,
                            columnWidth: widthColumn,
                            distributed: value == 3 || status == 1 ? true : false ,
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    xaxis: {
                        categories: categories,
                    }
                };

                var chart = new ApexCharts(document.querySelector(element), options);
                chart.render();
            }
            else
            {
                $(element).html('<div class="alert alert-primary" role="alert"><div class="alert-body">Seçtiğiniz seçeneğe göre veri bulunamadı.</div></div>');
            }
        }
    });
    /*
        var options = {
            series: [{
                data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    borderRadius: 4,
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
                    'United States', 'China', 'Germany'
                ],
            }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();

     */
}

function speedMeter2(key,label)
{
    new JustGage({
        id: key,
        value: 75,
        min: 0,
        max: 100,
        gaugeWidthScale: 0.6,
        customSectors: {
            ranges: [{
                color: "#43bf58",
                lo: 0,
                hi: 50
            }, {
                color: "#ff3b30",
                lo: 51,
                hi: 100
            }]
        },
        counter: true
    });
}

function speedMeter3(key,label)
{
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "post",
        dataType: "json",
        url: '/speedMeter',
        data: {
            _token: token,
            key: key
        },
        success: function(response)
        {
            if (response.result == 1)
            {

                var responseJson  = JSON.parse(response.response);
                var data          = responseJson.factory_value;

                $('#'+key).html('');
                new JustGage({
                    id: key,
                    value: data,
                    min: 0,
                    max: 200,
                    gaugeWidthScale: 0.6,
                    customSectors: {
                        ranges: [
                            {
                                color: "#ff3b30",
                                lo: 0,
                                hi: 50
                            },
                            {
                                color: "#F9A825",
                                lo: 51,
                                hi: 150
                            },
                            {
                                color: "#43bf58",
                                lo: 151,
                                hi: 200
                            }]
                    },
                    counter: false
                });
            }
        }
    });
}

$(function () {
    'use strict';
    var modernWizard = document.querySelector('.modern-wizard-example');

    var modernStepper = new Stepper(modernWizard, {
        linear: false
    });
    $(modernWizard)
        .find('.btn-next')
        .on('click', function () {
            modernStepper.next();
        });
    $(modernWizard)
        .find('.btn-prev')
        .on('click', function () {
            modernStepper.previous();
        });

    $(modernWizard)
        .find('.btn-submit')
        .on('click', function () {
            alert('Submitted..!!');
        });
});
