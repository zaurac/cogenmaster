<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\FactoryValue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class StaterkitController extends Controller
{
    // home

    public function liveFactory()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Canlı Tesis İzleme'
                ]
            ];

        return view('content.live-factory', ['breadcrumbs' => $breadcrumbs]);
    }

    public function values()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Değerler'
                ]
            ];
        $values =
            [
                ['cylinder_temperature_l1','Silinidir Sıcaklığı L1'],
                ['cylinder_temperature_r1','Silinidir Sıcaklığı R1'],
                ['electric_work','Çalışma Saati'],
                ['electric_work_counter','Elektirik Üretim Sayacı'],
                ['exhaust_temperature_before_catalyst','Ortalama Egzoz Sıcaklığı 1'],
                ['exhaust_temperature_after_catalyst','Ortalama Egzoz Sıcaklığı 2'],
                ['gas_usage_total','Doğalgaz Kullanımı'],
                ['gen_active_power','Jeneratör Aktif Gücü'],
                ['gen_reactive_power','Jeneratör Reaktif Gücü'],
                ['operating_hours_counter','Elektriksel Güç (kWh)'],
                ['operation_hours','Motor Çalışma Saati'],
                ['power_factor','Şebeke Güç Faktörü'],
                ['power_factor_cos_phi','Jeneratör Cos Phi Değeri'],
                ['setoint_power','Jeneratör Yük'],
                ['main_frekans','Şebeke Frekansı'],
                ['gen_frekans','Jeneratör Frekansı'],
                ['main_voltage','Şebeke Voltajı'],
                ['main_reactive_power','Şebeke Reaktif Güç'],
                //['chiller_water_inlet_temp','Chiller Suyu Giriş Sıcaklığı'],
                //['chiller_water_outlet_temp','Chiller Suyu Çıkış Sıcaklığı'],
                //['cooling_capasity','Soğutma Kapasitesi'],
                //['cooling_water_inlet','Soğutma Kulesi Giriş'],
                //['cooling_water_outlet','Soğutma Kulesi Çıkış'],
                ['heating_water_inlet_temp','HT Giriş'],
                ['heating_water_outlet_temp','HT Çıkış'],
                ['busbur_frekans','Busbur Frekansı'],
                ['busbur_voltage','Busbur Voltajı'],
                ['main_voltage_l1n','Şebeke Voltaj L1-N'],
                ['main_voltage_l2n','Şebeke Voltaj L2-N'],
                ['main_voltage_l3n','Şebeke Voltaj L3-N'],
                ['gen_voltage_l1n','Jeneratör Voltaj L1-N'],
                ['gen_voltage_l2n','Jeneratör Voltaj L2-N'],
                ['gen_voltage_l3n','Jeneratör Voltaj L3-N'],
                ['gen_current_1','Jeneratör Akım 1'],
                ['gen_current_2','Jeneratör Akım 2'],
                ['gen_current_3','Jeneratör Akım 3'],
                ['combustion_chamber_a1','Silindir Sıcaklığı A1'],
                ['combustion_chamber_a2','Silindir Sıcaklığı A2'],
                ['combustion_chamber_a3','Silindir Sıcaklığı A3'],
                ['combustion_chamber_a4','Silindir Sıcaklığı A4'],
                ['combustion_chamber_a5','Silindir Sıcaklığı A5'],
                ['combustion_chamber_a6','Silindir Sıcaklığı A6'],
                ['combustion_chamber_a7','Silindir Sıcaklığı A7'],
                ['combustion_chamber_a8','Silindir Sıcaklığı A8'],
                ['combustion_chamber_b1','Silindir Sıcaklığı B1'],
                ['combustion_chamber_b2','Silindir Sıcaklığı B2'],
                ['combustion_chamber_b3','Silindir Sıcaklığı B3'],
                ['combustion_chamber_b4','Silindir Sıcaklığı B4'],
                ['combustion_chamber_b5','Silindir Sıcaklığı B5'],
                ['combustion_chamber_b6','Silindir Sıcaklığı B6'],
                ['combustion_chamber_b7','Silindir Sıcaklığı B7'],
                ['combustion_chamber_b8','Silindir Sıcaklığı B8'],
                ['jacket_water_engine_inlet','Ceket Suyu Giriş'],
                ['jacket_water_engine_outlet','Ceket Suyu Çıkış'],
            ];

        return view('content.values',compact('breadcrumbs','values'));
    }

    public function maintenanceAndRepair()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Bakım ve Onarım'
                ]
            ];

        return view('content.maintenance-and-repair', ['breadcrumbs' => $breadcrumbs]);
    }

    public function reporting()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Raporlama'
                ]
            ];

        return view('content.reporting', ['breadcrumbs' => $breadcrumbs]);
    }

    public function advices()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Tavsiyeler'
                ]
            ];

        return view('content.advices', ['breadcrumbs' => $breadcrumbs]);
    }

    public function configuration()
    {
        $breadcrumbs =
            [
                [
                    'name' => "Ana Sayfa",
                    'link' => 'home'
                ],
                [
                    'name' => 'Konfigürasyon'
                ]
            ];

        return view('content.configuration', ['breadcrumbs' => $breadcrumbs]);
    }

    public function home()
    {
        $breadcrumbs = [
            [ 'name' => "Ana Sayfa"]
        ];

        //dd(Carbon::now()->add(-1,'hour')->format('Y-m-d H:i:s'));

        /*
        $strSQL= "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('temp',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('setoint_power',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('power_factor',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('gas_usage_total',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('electric_work',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('electric_work_counter',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('operating_hours_counter',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('exhaust_temperature_before_catalyst',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('cylinder_temperature_r1',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('cylinder_temperature_l1',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('power_factor_cos_phi',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('cooling_capasity',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('chiller_water_inlet_temp',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('chiller_water_outlet_temp',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('cooling_water_inlet',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('cooling_water_outlet',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('heatingwaterinlettemp',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('exhaust_temperature_after_catalyst',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('gen_active_power',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('generator_reactive_power',1,1);";
        $strSQL= $strSQL . "INSERT INTO factories_values (factory_key,factory_value,factory_id) VALUES ('heatingwateroutlettemp',1,1);";
        dd($strSQL);
        */

        return view('content.home', ['breadcrumbs' => $breadcrumbs]);
    }

    public function reactive()
    {
        $breadcrumbs = [
            [ 'name' => "Reaktif Güç Analizi"]
        ];

        return view('content.reactive', ['breadcrumbs' => $breadcrumbs]);
    }

    public function pageHtml($view)
    {
        $html = view($view)->render();
        return response()->json(['result' => 1,'response' => $html]);
    }

    public function lineChart(Request $request)
    {
        $attribute = array(
            'value'  => 'Durum',
            'key'    => 'Değer',
        );

        $rules = array(
            'value'  => 'required',
            'key'    => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attribute);

        if ($validator->fails())
        {
            return response()->json(['result' => 0,'response' => $validator],403);
        }

        $value     = $request->get('value');
        $name      = $request->get('name');
        $key       = $request->get('key');
        $status    = $request->get('status');
        $key       = explode(',',$key);
        $name      = explode(',',$name);
        $now       = Carbon::now();
        $startDate = $now->format('Y/m/d H:i:s');
        $endDate   = $now;

        if($value == 1)
        {
            $endDate   = $now->add(-1,'hour');
        }
        else if($value == 2)
        {
            $endDate   = $now->add(-24,'hour');
        }
        else if($value == 3)
        {
            $endDate   = $now->add(-24,'hour');
        }

        $endDate = $endDate->format('Y/m/d H:i:s');

        if ($endDate < $startDate)
        {
            if ($value == 1)
            {
                $startDate = Carbon::now()->add(-1,'hour')->format('Y/m/d H:i:s');
                $endDate   = Carbon::now()->format('Y/m/d H:i:s');
            }
            else if($value == 2)
            {
                $startDate = Carbon::now()->add(-24,'hour')->format('Y/m/d H:i:s');
                $endDate   = Carbon::now()->format('Y/m/d H:i:s');
            }
            else if($value == 3)
            {
                $startDate = Carbon::now()->add(-3,'minute')->format('Y/m/d H:i:s');
                $endDate   = Carbon::now()->format('Y/m/d H:i:s');
            }
        }

        if($value == 4)
        {
            $startDate = Carbon::now()->add(-1,'week')->format('Y/m/d H:i:s');
            $endDate   = Carbon::now()->format('Y/m/d H:i:s');
        }
        else if($value == 5)
        {
            $startDate = Carbon::now()->add(-1,'month')->format('Y/m/d H:i:s');
            $endDate   = Carbon::now()->format('Y/m/d H:i:s');
        }
        else if($value == 6)
        {
            $startDate = Carbon::now()->add(-1,'year')->format('Y/m/d H:i:s');
            $endDate   = Carbon::now()->format('Y/m/d H:i:s');
        }

        if ($value == 3 || $status == 1)
        {
            $factory_values = FactoryValue::select('factory_value','factory_key')->limit(count($key))->whereIn('factory_key',$key)->orderBy('id','desc')->get()->toArray();
        }
        else
        {
            $factory_values = FactoryValue::selectRaw("factory_value,FORMAT(created_at, 'MM/dd HH:mm:ss') as time,factory_key")->whereRaw("FORMAT(created_at, 'yyyy/MM/dd HH:mm:ss') BETWEEN '".$startDate."' AND '".$endDate."'")->whereIn('factory_key',$key)->orderBy('id','desc')->get()->toArray();
        }

        if (count($factory_values) > 0)
        {
            $response = $factory_values;
        }
        else
        {

            foreach ($key as $ke)
            {
                $response[] = ['factory_value'=> 0,'time' => '00/00 00:00:00','factory_key' => $ke];
            }
        }

        $names  = [];
        $colors = [];

        for ($a = 0; $a < count($key); $a++)
        {
            $names[$key[$a]] = $name[$a];
            $colors[]        = Helper::random_color();
        }


        foreach ($response as $order => $value)
        {
            $data[$value['factory_key']]['name']   = $names[$value['factory_key']];
            $data[$value['factory_key']]['data'][] = $value['factory_value'];
        }


        return response()->json(
            [
                'result'   => 1,
                'response' => json_encode($response),
                'series'   => json_encode(array_values($data)),
                'colors'   => json_encode($colors),
            ]
        );
    }

    public function speedMeter(Request $request)
    {
        try
        {
            $attribute = array(
                'key'    => 'Değer',
            );

            $rules = array(
                'key'    => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($attribute);

            if ($validator->fails())
            {
                return response()->json(['result' => 0,'response' => $validator],403);
            }

            $key            = $request->get('key');
            $factory_values = FactoryValue::select('factory_value','factory_key')->where('factory_key',$key)->orderBy('id','desc')->first();

            return response()->json(
                [
                    'result'   => 1,
                    'response' => !empty($factory_values) ? json_encode($factory_values) : json_encode(['factory_value'=> 0,'factory_key'=> $key])
                ]
            );
        }
        catch (\Exception $e)
        {
            return response()->json(['result' => 0,'response' => $e],403);
        }
    }

    // Layout collapsed menu
    public function collapsed_menu()
    {
        $pageConfigs = ['sidebarCollapsed' => true];
        $breadcrumbs = [
            ['link' => "home", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Layouts"], ['name' => "Collapsed menu"]
        ];
        return view('/content/layout-collapsed-menu', ['breadcrumbs' => $breadcrumbs, 'pageConfigs' => $pageConfigs]);
    }

    // layout boxed
    public function layout_full()
    {
        $pageConfigs = ['layoutWidth' => 'full'];

        $breadcrumbs = [
            ['link' => "home", 'name' => "Home"], ['name' => "Layouts"], ['name' => "Layout Full"]
        ];
        return view('/content/layout-full', ['pageConfigs' => $pageConfigs, 'breadcrumbs' => $breadcrumbs]);
    }

    // without menu
    public function without_menu()
    {
        $pageConfigs = ['showMenu' => false];
        $breadcrumbs = [
            ['link' => "home", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Layouts"], ['name' => "Layout without menu"]
        ];
        return view('/content/layout-without-menu', ['breadcrumbs' => $breadcrumbs, 'pageConfigs' => $pageConfigs]);
    }

    // Empty Layout
    public function layout_empty()
    {
        $breadcrumbs = [['link' => "home", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Layouts"], ['name' => "Layout Empty"]];
        return view('/content/layout-empty', ['breadcrumbs' => $breadcrumbs]);
    }
    // Blank Layout
    public function layout_blank()
    {
        $pageConfigs = ['blankPage' => true];
        return view('/content/layout-blank', ['pageConfigs' => $pageConfigs]);
    }
}
